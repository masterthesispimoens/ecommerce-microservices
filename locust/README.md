# Load Testing with Locust.io

This section includes required LocustFile.py defining a task sequence to generate a number of using accessing the application.

![task-sequence-schema](schema-locust.png)

## Running the test

1. Install Locust by running ```pip3 install -r requirements.txt```.
2. Run ```locust --host=<http://localhost:5000>```.
3. Open http://localhost:8089 in your browser to start the test and view the results.
