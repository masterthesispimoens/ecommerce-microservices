from locust import HttpLocust, TaskSequence, seq_task, task
from random import choice, randint

from models import Customer, Article

# https://github.com/locustio/locust/issues/646


class UserBehavior(TaskSequence):

    def __init__(self, *args, **kwargs):
        super(UserBehavior, self).__init__(*args, **kwargs)

        self.customer = Customer().randomize()

    def on_start(self):
        self.register()
        self.login()

    def on_stop(self):
        self.cleanup()

    def register(self):
        self.client.post('/user/register', data=self.customer.get_json())

    def login(self):
        self.client.post('/user/login',
                         data={'username': self.customer.username,
                               'password': self.customer.password
                               })

    def cleanup(self):
        self.client.post('/user/delete-account')

    @seq_task(1)
    @task(3)
    def add_to_catalog(self):
        article = Article()
        article.randomize()

        self.client.post('/create-article', data=article.get_json())

    @seq_task(2)
    @task(5)
    def add_to_cart(self):
        headers = {'accept': 'application/json'}

        with self.client.get('/', headers=headers) as response:
            articles = response.json()
            random_article = choice(articles)
            random_article_id = int(random_article['article_id'])

            self.client.post('/cart/add-article/{}'.format(random_article_id),
                             data={'article_id': random_article_id,
                                   'quantity': randint(1, random_article['quantity'])
                                   })

    @seq_task(3)
    @task(1)
    def checkout(self):
        self.client.post('/checkout')


class User(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
