from random import choice, randint
from string import ascii_letters


class Customer:

    def __init__(self, **kwargs):
        try:
            self.username = kwargs['username']
            self.name = kwargs['name']
            self.email = kwargs['email']
            self.password = kwargs['password']
        except KeyError:
            pass

    def randomize(self):
        self.username = ''.join(choice(ascii_letters) for i in range(10))
        self.name = '{} {}'.format(''.join(choice(ascii_letters) for i in range(5)),
                                   ''.join(choice(ascii_letters) for i in range(5)))
        self.email = '{}@locust-test.io'.format(self.username)
        self.password = ''.join(choice(ascii_letters) for i in range(10))

        return self

    def get_json(self):
        return {
            'username': self.username,
            'name': self.name,
            'email': self.email,
            'password': self.password
        }


class Article:

    def __init__(self, **kwargs):
        try:
            self.title = kwargs['title']
            self.description = kwargs['description']
            self.image = kwargs['image']
            self.price = kwargs['price']
            self.quantity = kwargs['quantity']
        except KeyError:
            pass

    def randomize(self):
        self.title = ''.join(choice(ascii_letters) for i in range(15))
        self.description = ''
        self.image = ''
        self.price = randint(1, 2000)
        self.quantity = randint(5, 200)

    def get_json(self):
        return {
            'title': self.title,
            'description': self.description,
            'image': self.image,
            'price': self.price,
            'quantity': self.quantity
        }


if __name__ == '__main__':
    randy = Customer()
    randy.randomize()
    print(randy.get_json())

    fixy_credentials = {
        'username': 'imfixy',
        'name': 'Joe Fixy',
        'email':  'imfixy@elocust-test.io',
        'password': 'fixyspassword'
    }
    fixy = Customer(**fixy_credentials)
    print(fixy.get_json())

    article = Article()
    article.randomize()
    print(article.get_json())
