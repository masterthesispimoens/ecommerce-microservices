from flask import request
from flask_restplus import Resource, abort, fields

from app import api, db, nice_json
from app.models import Customer

# TODO: https://aviaryan.com/blog/gsoc/restplus-validation-custom-fields

customer_model = api.model('Customer', {
    'username': fields.String(required=True, example='pimoens'),
    'name': fields.String(required=True, example='Pieter Moens'),
    'email': fields.String(required=True, example='Pieter.Moens@UGent.be'),
    'password': fields.String(required=True, example='5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
                              min_length=64, max_length=64)
})


@api.route('/customers')
class CustomersResource(Resource):
    def get(self):
        customers = Customer.query.all()
        response = [customer.get_json() for customer in customers]
        return nice_json(response)

    @api.expect(customer_model, validate=True)
    @api.response(201, 'Customer created successfully')
    @api.response(400, 'Bad request')
    @api.response(409, 'Already exists')
    def post(self):
        customer = request.json

        _customer = Customer.query.filter_by(username=customer['username']).first()
        if _customer is not None:
                return abort(409, 'A customer with this username already exists')

        _customer = Customer.query.filter_by(email=customer['email']).first()
        if _customer is not None:
                return abort(409, 'A customer with this email address already exists')

        customer = Customer(username=customer['username'],
                            name=customer['name'],
                            email=customer['email'],
                            password=customer['password'])
        db.session.add(customer)
        db.session.commit()

        customers = Customer.query.all()
        response = [customer.get_json() for customer in customers]
        return nice_json(response, 201)


@api.route('/customer/<username>')
@api.doc(params={'username': 'A customer username'})
class CustomerResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Customer not found')
    def get(self, username):
        customer = Customer.query.filter_by(username=username).first()
        if customer is not None:
            return nice_json(customer.get_json())
        return abort(404)

    @api.expect(customer_model, validate=True)
    @api.response(200, 'Success')
    @api.response(400, 'Bad request')
    @api.response(404, 'Customer not found')
    def put(self, username):
        customer = request.json

        if Customer.query.filter_by(username=username).update(customer):
            db.session.commit()
            _customer = Customer.query.filter_by(username=username).first()
            return nice_json(_customer.get_json())
        return abort(404)

    @api.response(200, 'Success')
    @api.response(404, 'Customer not found')
    def delete(self, username):
        customer = Customer.query.filter_by(username=username).first()
        if customer is not None:
            db.session.delete(customer)
            db.session.commit()

            customers = Customer.query.all()
            response = [customer.get_json() for customer in customers]
            return nice_json(response)
        return abort(404)
