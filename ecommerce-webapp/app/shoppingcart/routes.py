from flask import Blueprint, abort, session, url_for, render_template, request, redirect
from flask_login import current_user
from werkzeug.exceptions import ServiceUnavailable

import requests

from app import app
from app.config import Services

blueprint = Blueprint('cart', __name__, static_folder='static', template_folder='templates', url_prefix='/cart')


def load_cart():
    """
    Fetch and update shopping cart for the current session

    :return: Shopping cart
    """
    if 'cart' in session:
        cart = session['cart']
    else:
        if current_user.is_authenticated:
            try:
                cart = requests.get(Services.CART.value + '/cart/{}'.format(current_user.id))
            except requests.ConnectionError:
                raise ServiceUnavailable('Shopping cart service is unavailable.')

            if cart.status_code == 404:
                cart = requests.post(Services.CART.value + '/carts', json={'customer_id': current_user.id})

            cart = cart.json()
            session['cart'] = cart
        else:
            cart = {'articles': []}
            session['cart'] = cart
    return cart


def merge_cart(customer_id):
    """
    Fetch cart stored in session and merge with client shopping cart

    :return: Shopping cart
    """
    if 'cart' in session:
        cart = session['cart']

        try:
            response = requests.post(Services.CART.value + '/cart/{}/merge'.format(customer_id), json=cart)

            if response.status_code == 200:
                session['cart'] = response.json()
                return True
        except requests.ConnectionError:
            raise ServiceUnavailable('Shopping cart service is unavailable.')

    return False


@app.context_processor
def cart_processor():
    """
    https://stackoverflow.com/questions/6036082/call-a-python-function-from-jinja2/22966127#22966127
    """
    return dict(load_cart=load_cart)


@blueprint.route('/')
def index():
    cart = load_cart()

    _cart = {
        'articles': [],
        'total_price': 0
    }
    for _article in cart['articles']:
        try:
            article = requests.get(Services.CATALOG.value + '/catalog/{}'.format(_article['article_id']))

            if article.status_code != 404:
                article = article.json()

                article['article_id'] = _article['article_id']
                article['quantity'] = _article['quantity']
                article['total_price'] = article['quantity'] * article['price']

                _cart['total_price'] += article['total_price']
                _cart['articles'].append(article)
        except requests.ConnectionError:
            raise ServiceUnavailable('Catalog service is unavailable.')

    return render_template('cart.html', cart=_cart)


@blueprint.route('/add-article/<article_id>', methods=['POST'])
def add_cart_article(article_id):
    cart = load_cart()
    article = {'article_id': int(article_id), 'quantity': 1}

    if current_user.is_authenticated:
        try:
            response = requests.post(Services.CART.value + '/cart/{}'.format(current_user.id), json=article)

            if response.status_code != 201:
                return abort(response.status_code)
            session['cart'] = response.json()
        except requests.ConnectionError:
            raise ServiceUnavailable('Shopping cart service is unavailable.')
    else:
        article_exists = False
        for _article in cart['articles']:
            if _article['article_id'] == article_id:
                _article['quantity'] += article['quantity']
                article_exists = True

        if not article_exists:
            cart['articles'].append(article)

    return redirect(url_for('cart.index'))


@blueprint.route('/delete-article/<int:article_id>', methods=['POST'])
def delete_cart_article(article_id):
    cart = load_cart()

    if current_user.is_authenticated:
        try:
            response = requests.delete(Services.CART.value + '/cart/{}/article/{}'.format(current_user.id, article_id))

            if response.status_code != 200:
                return abort(response.status_code)
            session['cart'] = response.json()
        except requests.ConnectionError:
            raise ServiceUnavailable('Shopping cart service is unavailable.')

    else:
        for article in cart['articles']:
            if article['article_id'] == article_id:
                cart['articles'].remove(article)

    return redirect(url_for('cart.index'))
