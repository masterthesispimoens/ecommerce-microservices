from flask import Flask, request, make_response, json
from flask_login import LoginManager

from importlib import import_module

from app.config import Config


def nice_json(arg, status_code=200):
    response = make_response(json.dumps(arg, sort_keys=True, indent=4), status_code)
    response.headers['Content-type'] = "application/json"
    return response


def request_wants_json():
    """
    Helper function for content negotiation
    :return: Accept application/json header present
    """
    best = request.accept_mimetypes \
        .best_match(['application/json', 'text/html'])
    return best == 'application/json' and \
           request.accept_mimetypes[best] > \
           request.accept_mimetypes['text/html']


def register_blueprints(app):
    for module_name in ['ecommerce', 'user', 'shoppingcart']:
        blueprint_module = import_module('app.{}.routes'.format(module_name))
        app.register_blueprint(blueprint_module.blueprint)


app = Flask(__name__, static_folder='./ecommerce/static')
app.config.from_object(Config)

login_manager = LoginManager()
login_manager.init_app(app)

register_blueprints(app)
