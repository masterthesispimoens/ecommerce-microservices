from enum import Enum


class Config(object):
    SECRET_KEY = 'this_is_secret!'


class Services(Enum):
    CUSTOMER = 'http://customer-microservice:5010'
    CATALOG = 'http://catalog-microservice:5011'
    CART = 'http://cart-microservice:5012'
    ORDER = 'http://order-microservice:5013'

    def __str__(self):
        return self.value
