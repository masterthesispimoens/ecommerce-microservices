import requests
from werkzeug.exceptions import ServiceUnavailable
import hashlib

from flask import abort, Blueprint, flash, render_template, redirect, request, url_for, session
from flask_login import current_user, login_user, logout_user

from app import login_manager
from app.config import Services
from app.models import Customer
from app.shoppingcart.routes import merge_cart


blueprint = Blueprint('user', __name__, template_folder='templates', url_prefix='/user')


@login_manager.user_loader
def load_user(customer_id):
    try:
        customer = requests.get(Services.CUSTOMER.value + '/customer/{}'.format(customer_id))
    except requests.ConnectionError:
        return None

    if customer.status_code == 404:
        return None
    return Customer(customer.json())


@blueprint.route('/<user_id>')
def profile(user_id):
    user = load_user(user_id)
    return render_template('profile.html', user=user)


@blueprint.route('/account')
def account():
    if current_user.is_authenticated:
        return render_template('account.html')
    else:
        return redirect(url_for('ecommerce.index'))


@blueprint.route('/articles')
def articles():
    if current_user.is_authenticated:
        try:
            response = requests.get(Services.CATALOG.value + '/seller/{}'.format(current_user.username))

            if response.status_code != 200:
                abort(response.status_code)
        except requests.ConnectionError:
            raise ServiceUnavailable('Catalog service is unavailable.')

        articles = response.json()

        return render_template('articles.html', articles=articles)
    else:
        return redirect(url_for('ecommerce.index'))


@blueprint.route('/orders')
def orders():
    if current_user.is_authenticated:
        try:
            response = requests.get(Services.ORDER.value + '/customer/{}'.format(current_user.username))
            inbound = response.json() if response.status_code == 200 else {}

            response = requests.get(Services.ORDER.value + '/seller/{}'.format(current_user.username))
            outbound = response.json() if response.status_code == 200 else {}
        except requests.ConnectionError:
            raise ServiceUnavailable('Order service is unavailable.')

        return render_template('orders.html', inbound=inbound, outbound=outbound)
    else:
        return redirect(url_for('ecommerce.index'))


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    # Check if an user is already logged in.
    if current_user.is_authenticated:
        return redirect(url_for('ecommerce.index'))

    # Validate login request
    if request.method == 'POST':
        customer_username = request.form['username']
        customer_password = request.form['password']

        # Connect to customer service
        try:
            customer = requests.get(Services.CUSTOMER.value + '/customer/{}'.format(customer_username))
        except requests.ConnectionError:
            raise ServiceUnavailable('Customer service is unavailable.')

        if customer.status_code == 404:
            flash('No customer exists with this ID.', 'danger')
        else:
            customer = Customer(customer.json())
            password_hash = hashlib.sha256(customer_password.encode('UTF-8')).hexdigest()

            if customer.password == password_hash:
                login_user(customer)
                merge_cart(customer.id)
                return redirect(url_for('ecommerce.index'))
            else:
                flash('Invalid password.', 'danger')

    return render_template('login.html')


@blueprint.route('/register', methods=['GET', 'POST'])
def register():
    # Check if an user is already logged in.
    if current_user.is_authenticated:
        return redirect(url_for('ecommerce.index'))

    # Validate login request
    if request.method == 'POST':
        customer_data = {
            'username': request.form['username'],
            'name': request.form['name'],
            'email': request.form['email'],
            'password': hashlib.sha256(request.form['password'].encode('UTF-8')).hexdigest()
        }

        try:
            customer = requests.post(Services.CUSTOMER.value + '/customers', json=customer_data)
        except requests.ConnectionError:
            raise ServiceUnavailable('Customer service is unavailable.')

        if 'error' in customer.json():
            flash(customer.json()['error'], 'danger')
        else:
            flash('Registration successful! You can now login.', 'success')
            return redirect(url_for('user.login'))

    return render_template('register.html')


@blueprint.route('/logout')
def logout():
    logout_user()
    session.clear()
    return redirect(url_for('ecommerce.index'))


@blueprint.route('/delete-account', methods=['POST'])
def delete_account():
    # Check if an user is logged in.
    if not current_user.is_authenticated:
        return abort(400)

    # Delete the user's shopping cart
    try:
        response = requests.delete(Services.CART.value + '/cart/{}'.format(current_user.username))

        if response.status_code != 200:
            return abort(response.status_code)
    except requests.ConnectionError:
        raise ServiceUnavailable('Shopping cart service is unavailable.')

    # Delete all articles being sold by the user
    try:
        response = requests.delete(Services.CATALOG.value + '/seller/{}'.format(current_user.username))

        if response.status_code != 200:
            return abort(response.status_code)
    except requests.ConnectionError:
        raise ServiceUnavailable('Catalog service is unavailable.')

    # TODO: Delete this for production environments !
    #  Only meant to keep database clean when generating random orders during testing
    # Delete all orders from the user
    try:
        response = requests.delete(Services.ORDER.value + '/customer/{}'.format(current_user.username))
        if response.status_code != 200:
            return abort(response.status_code)

        response = requests.delete(Services.ORDER.value + '/seller/{}'.format(current_user.username))
        if response.status_code != 200:
            return abort(response.status_code)
    except requests.ConnectionError:
        raise ServiceUnavailable('Order service is unavailable.')

    # Delete the user account
    try:
        response = requests.delete(Services.CUSTOMER.value + '/customer/{}'.format(current_user.username))

        if response.status_code != 200:
            return abort(response.status_code)
    except requests.ConnectionError:
        raise ServiceUnavailable('Customer service is unavailable.')

    return redirect(url_for('user.logout'))
