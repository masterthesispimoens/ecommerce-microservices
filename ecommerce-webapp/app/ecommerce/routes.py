from werkzeug.exceptions import ServiceUnavailable
import requests

from flask import abort, Blueprint, flash, render_template, redirect, url_for, request, session
from flask_login import current_user

from app import request_wants_json, nice_json
from app.config import Services


blueprint = Blueprint('ecommerce', __name__, static_folder='static', template_folder='templates', url_prefix='')


@blueprint.route('/')
def index():
    print(Services.CATALOG.value)
    try:
        response = requests.get(Services.CATALOG.value + '/catalog')
    except requests.ConnectionError:
        raise ServiceUnavailable('Catalog service is unavailable.')

    if request_wants_json():
        return nice_json(response.json())
    else:
        return render_template('index.html', catalog=response.json())


@blueprint.route('/create-article', methods=['POST'])
def create_article():
    if current_user.is_authenticated:
        # TODO: Form validation !
        article = {
            'title': request.form['title'],
            'description': request.form['description'],
            'price': float(request.form['price']),
            'quantity': int(request.form['quantity']),
            'seller_id': current_user.username
        }

        # TODO: Support images
        # image = request.files['image']

        try:
            response = requests.post(Services.CATALOG.value + '/catalog', json=article)

            if response.status_code != 201:
                return abort(response.status_code)
        except requests.ConnectionError:
            raise ServiceUnavailable('Catalog service is unavailable.')

        flash('Article has been created successfully.', 'success')
        return redirect(url_for('user.articles'))
    else:
        return redirect(url_for('ecommerce.index'))


@blueprint.route('/thankyou')
def thank_you():
    return render_template('thankyou.html')


@blueprint.route('/checkout', methods=['POST'])
def checkout():
    if current_user.is_authenticated:
        if 'cart' in session:
            cart = session['cart']
            orders = {}
            for _article in cart['articles']:
                try:
                    response = requests.get(Services.CATALOG.value + '/catalog/{}'.format(_article['article_id']))

                    if response.status_code != 200:
                        return abort(response.status_code)
                except requests.ConnectionError:
                    raise ServiceUnavailable('Catalog service is unavailable.')

                article = response.json()
                seller_id = article['seller_id']
                if seller_id in orders:
                    orders[seller_id].append({
                        'article_id': _article['article_id'],
                        'quantity': _article['quantity']
                    })
                else:
                    orders[seller_id] = [{
                        'article_id': _article['article_id'],
                        'quantity': _article['quantity']
                    }]

            for seller_id in orders:
                order = {
                    'customer_id': current_user.username,
                    'seller_id': seller_id,
                    'articles': orders[seller_id]
                }

                try:
                    response = requests.post(Services.ORDER.value + '/orders', json=order)

                    if response.status_code != 201:
                        return abort(response.status_code)
                except requests.ConnectionError:
                    raise ServiceUnavailable('Order Management service is unavailable.')

            try:
                response = requests.post(Services.CART.value + '/cart/{}/empty'.format(current_user.username))

                if response.status_code != 200:
                    return abort(response.status_code)

                session.pop('cart')
            except requests.ConnectionError:
                raise ServiceUnavailable('Shopping cart service is unavailable.')

            return redirect(url_for('ecommerce.thank_you'))
    else:
        flash('Please login to continue checking out.', 'warning')
        return redirect(url_for('user.login'))
