from flask_login import UserMixin
from jsonschema import validate, ValidationError


class Customer(UserMixin):

    def __init__(self, customer):
        schema = {
            'type': 'object',
            'properties': {
                'username': {'type': 'string'},
                'name': {'type': 'string'},
                'email': {'type': 'string'},
                'password': {'type': 'string', 'minLength': 64, 'maxLength': 64}
            }
        }

        validate(customer, schema)
        self.username = customer['username']
        self.name = customer['name']
        self.email = customer['email']
        self.password = customer['password']

    @property
    def id(self):
        return self.username
