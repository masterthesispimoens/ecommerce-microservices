#!/usr/bin/env bash

#kubectl create ns ecommerce-webapp
#kubectl label ns ecommerce-webapp istio-injection=enabled
#
#kubectl create ns customer-microservice
#kubectl label ns customer-microservice istio-injection=enabled
#
#kubectl create ns catalog-microservice
#kubectl label ns catalog-microservice istio-injection=enabled
#
#kubectl create ns cart-microservice
#kubectl label ns cart-microservice istio-injection=enabled
#
#kubectl create ns order-microservice
#kubectl label ns order-microservice istio-injection=enabled
#
#helm upgrade --install ecommerce-webapp ./ecommerce-webapp --namespace ecommerce-webapp
#helm upgrade --install customer-microservice ./customer-microservice --namespace customer-microservice
#helm upgrade --install catalog-microservice ./catalog-microservice --namespace catalog-microservice
#helm upgrade --install cart-microservice ./shoppingcart-microservice --namespace cart-microservice
#helm upgrade --install order-microservice ./order-microservice --namespace order-microservice
#
#kubectl expose deployment ecommerce-webapp --type=NodePort --name=ecommerce-webapp-port -n ecommerce-webapp
#kubectl expose deployment customer-microservice --type=NodePort --name=customer-service-port -n customer-microservice
#kubectl expose deployment catalog-microservice --type=NodePort --name=catalog-service-port -n catalog-microservice
#kubectl expose deployment cart-microservice --type=NodePort --name=cart-service-port -n cart-microservice
#kubectl expose deployment order-microservice --type=NodePort --name=order-service-port -n order-microservice
#
#kubectl apply -f istio-gateway.yaml -n ecommerce-webapp

kubectl create ns ecommerce-platform
kubectl label ns ecommerce-platform istio-injection=enabled

helm upgrade --install ecommerce-webapp ./ecommerce-webapp --namespace ecommerce-platform
helm upgrade --install customer-microservice ./customer-microservice --namespace ecommerce-platform
helm upgrade --install catalog-microservice ./catalog-microservice --namespace ecommerce-platform
helm upgrade --install cart-microservice ./shoppingcart-microservice --namespace ecommerce-platform
helm upgrade --install order-microservice ./order-microservice --namespace ecommerce-platform

kubectl expose deployment ecommerce-webapp --type=NodePort --name=ecommerce-webapp-port -n ecommerce-platform
kubectl expose deployment customer-microservice --type=NodePort --name=customer-service-port -n ecommerce-platform
kubectl expose deployment catalog-microservice --type=NodePort --name=catalog-service-port -n ecommerce-platform
kubectl expose deployment cart-microservice --type=NodePort --name=cart-service-port -n ecommerce-platform
kubectl expose deployment order-microservice --type=NodePort --name=order-service-port -n ecommerce-platform

kubectl apply -f istio-gateway.yaml -n ecommerce-platform

kubectl expose deployment kiali --type=NodePort --port=20001 --name=kiali-port -n istio-system
kubectl expose deployment grafana --type=NodePort --port=3000 --name=grafana-port -n istio-system
