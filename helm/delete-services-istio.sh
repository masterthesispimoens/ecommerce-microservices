#!/usr/bin/env bash

#kubectl delete namespaces ecommerce-webapp
#kubectl delete namespaces customer-microservice
#kubectl delete namespaces catalog-microservice
#kubectl delete namespaces cart-microservice
#kubectl delete namespaces order-microservice

kubectl delete namespaces ecommerce-platform

helm del --purge ecommerce-webapp
helm del --purge customer-microservice
helm del --purge catalog-microservice
helm del --purge cart-microservice
helm del --purge order-microservice