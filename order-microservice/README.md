
# Order Management Microservice API [v1.0]
Order Management microservice for the e-commerce microservices architecture

## API Endpoints

### /customer/{customer_id}


#### DELETE


##### Summary
Delete all orders from a customer



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Customer not found |  |

#### GET


##### Summary
Retrieve all orders from a customer



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Customer not found |  |

### /order/{order_id}


#### DELETE


##### Summary
Delete an order by ID



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Order not found |  |

#### GET


##### Summary
Retrieve an order by ID



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Order not found |  |

#### PUT




##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Order](#order) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 200 | Success |  |
| 404 | Order not found |  |

### /order/{order_id}/status


#### PUT


##### Summary
Edit the status of an order


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Status](#status) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 200 | Success |  |
| 404 | Order not found |  |

#### GET


##### Summary
Retrieve the status of an order



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Order not found |  |

### /orders

#### GET


##### Summary
Retrieve all orders



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |

#### POST


##### Summary
Create new order


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Order](#order) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 201 | Order created successfully |  |

### /seller/{seller_id}


#### DELETE


##### Summary
Delete all orders from a seller



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Seller not found |  |

#### GET


##### Summary
Retrieve all orders from a seller



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Seller not found |  |

## API Schemas

### Status

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| status | string |  | No |  |

### Article

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| quantity | integer |  | Yes |  |
| article_id | integer |  | Yes |  |

### Order

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| articles | array |  | Yes |  |
| seller_id | string |  | Yes | pimoens |
| status | string |  | No |  |
| customer_id | string |  | Yes | pimoens |
