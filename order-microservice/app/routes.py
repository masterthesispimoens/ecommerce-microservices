import requests
from werkzeug.exceptions import ServiceUnavailable

from flask import abort, request
from flask_restplus import Resource, fields

from app import api, db, nice_json
from app.config import Services
from app.models import Order, Article

article_model = api.model('Article', {
    'article_id': fields.Integer(required=True, min=1),
    'quantity': fields.Integer(required=True, min=1)
})

order_model = api.model('Order', {
    'customer_id': fields.String(required=True, example='pimoens'),
    'seller_id': fields.String(required=True, example='pimoens'),
    'status': fields.String(),
    'articles': fields.List(fields.Nested(article_model), required=True)
})

status_model = api.model('Status', {
    'status': fields.String()
})


@api.route('/orders')
class OrdersResource(Resource):
    @api.response(200, 'Success')
    def get(self):
        """
        Retrieve all orders
        """
        orders = Order.query.all()
        response = [order.get_json() for order in orders]
        return nice_json(response)

    @api.expect(order_model)
    @api.response(201, 'Order created successfully')
    @api.response(400, 'Bad request')
    def post(self):
        """
        Create new order
        """
        order = request.json

        status = order['status'] if 'status' in order else 'OPEN'
        _order = Order(customer_id=order['customer_id'], seller_id=order['seller_id'], status=status)
        db.session.add(_order)
        db.session.flush()

        for article in order['articles']:
            article_id = article['article_id']
            quantity = article['quantity']
            _article = Article(article_id=article_id, quantity=quantity, order_id=_order.id)
            db.session.add(_article)

            try:
                response = requests.get(Services.CATALOG.value + '/catalog/{}'.format(article_id))
                if response.status_code != 200:
                    return abort(response.status_code)

                o_article = response.json()
                response = requests.put(Services.CATALOG.value + '/catalog/{}/quantity'.format(article_id),
                                        json={'quantity': o_article['quantity'] - quantity}
                                        )

                if response.status_code != 200:
                    return abort(response.status_code)
            except requests.ConnectionError:
                raise ServiceUnavailable('Catalog service is unavailable.')

        db.session.commit()
        order = Order.query.filter_by(id=_order.id).first()
        return nice_json(order.get_json(), 201)


@api.route('/order/<order_id>')
@api.doc(params={'order_id': 'An order ID'})
class OrderResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Order not found')
    def get(self, order_id):
        """
        Retrieve an order by ID
        """
        order = Order.query.filter_by(id=order_id).first()

        if order is None:
            abort(404)
        return nice_json(order.get_json(), 200)

    @api.expect(order_model, validate=True)
    @api.response(200, 'Success')
    @api.response(400, 'Bad request')
    @api.response(404, 'Order not found')
    def put(self, order_id):
        order = request.json

        if Order.query.filter_by(id=order_id).update(order):
            db.session.commit()
            _order = Order.query.filter_by(id=order_id).first()
            return nice_json(_order.get_json())
        return abort(404)

    @api.response(200, 'Success')
    @api.response(404, 'Order not found')
    def delete(self, order_id):
        """
        Delete an order by ID
        """
        order = Order.query.filter_by(id=order_id).first()
        if order is not None:
            db.session.delete(order)
            db.session.commit()

            orders = Order.query.all()
            response = [order.get_json() for order in orders]
            return nice_json(response)
        else:
            return abort(404)


@api.route('/order/<order_id>/status')
@api.doc(params={'order_id': 'An order ID'})
class OrderStatusResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Order not found')
    def get(self, order_id):
        """
        Retrieve the status of an order
        """
        order = Order.query.filter_by(id=order_id).first()

        if order is not None:
            return nice_json({'status': order.status})
        return abort(404)

    @api.expect(status_model)
    @api.response(200, 'Success')
    @api.response(400, 'Bad request')
    @api.response(404, 'Order not found')
    def put(self, order_id):
        """
        Edit the status of an order
        """
        status = request.json

        if Order.query.filter_by(id=order_id).update(status):
            db.session.commit()

            order = Order.query.filter_by(id=order_id).first()
            return nice_json(order.get_json())


@api.route('/customer/<customer_id>')
@api.doc(params={'customer_id': 'A customer ID'})
class CustomerResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Customer not found')
    def get(self, customer_id):
        """
        Retrieve all orders from a customer
        """
        orders = Order.query.filter_by(customer_id=customer_id).all()

        if orders is None:
            return abort(404)

        response = [order.get_json() for order in orders]
        return nice_json(response)

    @api.response(200, 'Success')
    @api.response(404, 'Customer not found')
    def delete(self, customer_id):
        """
        Delete all orders from a customer
        """
        orders = Order.query.filter_by(customer_id=customer_id).all()

        if orders is not None:
            for order in orders:
                db.session.delete(order)
            db.session.commit()

            orders = Order.query.all()
            response = [order.get_json() for order in orders]
            return nice_json(response)
        return abort(404)


@api.route('/seller/<seller_id>')
@api.doc(params={'seller_id': 'A seller ID'})
class SellerResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Seller not found')
    def get(self, seller_id):
        """
        Retrieve all orders from a seller
        """
        orders = Order.query.filter_by(seller_id=seller_id).all()

        if orders is None:
            return abort(404)

        response = [order.get_json() for order in orders]
        return nice_json(response)

    @api.response(200, 'Success')
    @api.response(404, 'Seller not found')
    def delete(self, seller_id):
        """
        Delete all orders from a seller
        """
        orders = Order.query.filter_by(seller_id=seller_id).all()

        if orders is not None:
            for order in orders:
                db.session.delete(order)
            db.session.commit()

            orders = Order.query.all()
            response = [order.get_json() for order in orders]
            return nice_json(response)
        return abort(404)
