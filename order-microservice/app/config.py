import os
from enum import Enum

POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
POSTGRES_DB = os.environ.get('POSTGRES_DB')
DATABASE_URI = 'postgresql://{USER}:{PASSWORD}@order-database:5432/{DATABASE}'.format(
    USER=POSTGRES_USER,
    PASSWORD=POSTGRES_PASSWORD,
    DATABASE=POSTGRES_DB)


class BaseConfig:
    """Base configuration"""
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    SQLALCHEMY_DATABASE_URI = DATABASE_URI


class ProductionConfig(BaseConfig):
    """Production configuration"""
    SQLALCHEMY_DATABASE_URI = DATABASE_URI


class Services(Enum):
    CATALOG = 'http://catalog-microservice:5011'

    def __str__(self):
        return self.value
