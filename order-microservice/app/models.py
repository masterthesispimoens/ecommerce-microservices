from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func

db = SQLAlchemy()


class Order(db.Model):

    __tablename__ = 'orders'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    customer_id = db.Column(db.String(255))
    seller_id = db.Column(db.String(255))
    created_date = db.Column(db.DateTime(timezone=True), default=func.now())
    updated_date = db.Column(db.DateTime(timezone=True), onupdate=func.now())
    status = db.Column(db.String(32))
    articles = db.relationship('Article', backref='orders', cascade="all, delete-orphan")

    def get_json(self):
        return {
            'order_id': self.id,
            'customer_id': self.customer_id,
            'seller_id': self.seller_id,
            'created_date': self.created_date,
            'updated_date': self.updated_date,
            'status': self.status,
            'articles': [article.get_json() for article in self.articles]
        }


class Article(db.Model):

    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    article_id = db.Column(db.Integer)
    quantity = db.Column(db.Integer, default=1)
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=False)

    def get_json(self):
        return {
            'article_id': self.article_id,
            'quantity': self.quantity
        }
