import argparse
import csv
import os
import re
import subprocess
from time import gmtime, sleep, strftime

TIMEFORMAT = "%Y-%m-%d %H:%M:%S"

HMS_RGX =  re.compile(r'(\d+(?:\.\d+)?)h(\d+(?:\.\d+)?)m(\d+(?:\.\d+)?)s')
MS_RGX = re.compile(r'(\d+(?:\.\d+)?)m(\d+(?:\.\d+)?)s')
S_RGX = re.compile(r'(\d+(?:\.\d+)?)s')
MILLIS_RGX = re.compile(r'(\d+(?:\.\d+)?)ms')
MICROS_RGX = re.compile(r'(\d+(?:\.\d+)?)µs')
PROCENT_RGX = re.compile(r'(\d+(?:\.\d+)?)%')


class Vegeta:

    def __init__(self, attacks='100@1m', targets='targets.txt', output='results.csv'):
        self.attacks = [{'rate': a[0], 'duration': a[1]} for a in [x.split('@') for x in attacks.split(' ')]]
        self.targets = targets
        if os.path.exists(output):
            os.remove(output)
        self.output = output

    def _get_attack_cmd(self, attack):
        #  vegeta attack -duration=5m -rate=200/1s -targets=targets.txt | tee results.bin
        return ['vegeta', 'attack',
                '-duration={duration}'.format(duration=attack['duration']),
                '-rate={rate}'.format(rate=attack['rate'] + '/1s'),
                '-targets={targets}'.format(targets=self.targets)]

    def _process_results(self):
        target_metrics = ['Latencies', 'Success']

        if not os.path.exists(self.output):
            with open(self.output, 'a+') as output:
                csv.writer(output).writerow(['Rate (ops)', 'Mean (ms)', 'P50 (ms)', 'P95 (ms)', 'P99 (ms)', 'Max (ms)', 'Success (%)'])

        for summary in os.listdir('tmp'):
            if summary.endswith('.txt'):
                rate = [summary.split('@')[0]]

                with open('tmp/' + summary, 'r') as f:
                    row = []

                    for line in f.readlines():
                        regex = re.match(r'([\w]+[\s]?[\w]+)[\s]+\[(.*)\][\s]+(.*)', line)

                        if regex is not None:
                            metric, labels, values = regex.groups()
                            labels = labels.split(', ')

                            values = values.split(', ')
                            processed_values = values
                            for i in range(len(values)):
                                dp = values[i]

                                value = HMS_RGX.match(dp)
                                if value is not None:
                                    processed_values[i] = (float(value.group(1)) * 3600 + float(
                                        value.group(2)) * 60 + float(value.group(3))) * 1000

                                value = MS_RGX.match(dp)
                                if value is not None:
                                    processed_values[i] = (float(value.group(1)) * 60 + float(value.group(2))) * 1000

                                value = S_RGX.match(dp)
                                if value is not None:
                                    processed_values[i] = float(value.group(1)) * 1000

                                value = MILLIS_RGX.match(dp)
                                if value is not None:
                                    processed_values[i] = float(value.group(1))

                                value = MICROS_RGX.match(dp)
                                if value is not None:
                                    processed_values[i] = float(value.group(1)) / 1000

                                value = PROCENT_RGX.match(dp)
                                if value is not None:
                                    processed_values[i] = float(value.group(1))

                            if metric in target_metrics:
                                row.extend(processed_values)

                    with open(self.output, 'a+') as output:
                        writer = csv.writer(output)
                        rate.extend(row)
                        writer.writerow(rate)
                        print(rate)

    def _average_results(self, times):
        results = {}
        with open(self.output, 'r') as output:
            reader = csv.reader(output, delimiter=',')
            count = 0
            for row in reader:
                if count > 0:
                    key = row[0]
                    if key in results.keys():
                        results[key] = [float(g) + float(h) for g, h in zip(results[key], row[1:])]
                    else:
                        results[key] = row[1:]
                count += 1

        for key in results.keys():
            results[key] = [float(value) / float(times) for value in results[key]]

        with open(self.output, 'a') as output:
            writer = csv.writer(output)
            writer.writerow(['--', 'AVERAGED', '--'])
            for key in results.keys():
                row = [key]
                row.extend(results[key])
                writer.writerow(row)

    def run(self, times=2, timeout=60):
        if not os.path.exists('tmp'):
            os.mkdir('tmp')

        for tmpfile in os.listdir('tmp'):
            file_path = os.path.join('tmp', tmpfile)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)

        for time in range(times):
            print('[*] ' + strftime(TIMEFORMAT, gmtime()) + ': Starting test N°' + str(time))

            for attack in self.attacks:
                print('[*] Starting attack with {} RPS'.format(attack['rate']))

                cmd = self._get_attack_cmd(attack)
                resultname = 'tmp/' + attack['rate'] + '@' + attack['duration'] + '.bin'
                subprocess.run(cmd, stdout=open(resultname, 'w'))

                cmd = ['vegeta', 'report', resultname]
                reportname = 'tmp/' + attack['rate'] + '@' + attack['duration'] + '.txt'
                subprocess.run(cmd, stdout=open(reportname, 'w'))

                print('[*] Attack successful')

            self._process_results()

            for tmpfile in os.listdir('tmp'):
                    file_path = os.path.join('tmp', tmpfile)
                    try:
                        if os.path.isfile(file_path):
                            os.unlink(file_path)
                    except Exception as e:
                        print(e)

            print('[*] ' + strftime(TIMEFORMAT, gmtime()) + ': Timeout')
            sleep(timeout)

        print('[*] ' + strftime(TIMEFORMAT, gmtime()) + ': Averaging results')
        self._average_results(times)
        print('[*] ' + strftime(TIMEFORMAT, gmtime()) + ': Test completed')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--duration', default=1, type=int, help='Duration for each attack (seconds)')
    parser.add_argument('--max_rate', default=200, type=int, help='Maximum OPS')
    parser.add_argument('--num_tests', default=10, type=int, help='Number of tests')
    parser.add_argument('--timeout', default=60, type=int, help='Time between tests (seconds)')

    args = parser.parse_args()

    DURATION = str(args.duration) + 's'
    RATES = [i if i != 0 else i + 1 for i in range(0, args.max_rate + 1, 10)]
    ATTACKS = ' '.join('{rate}@{duration}'.format(rate=rate, duration=DURATION) for rate in RATES)

    vegeta = Vegeta(attacks=ATTACKS)
    vegeta.run(times=args.num_tests, timeout=args.timeout)
