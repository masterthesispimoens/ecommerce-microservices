
# Catalog Microservice API [v1.0]
Catalog microservice for the e-commerce microservices architecture

## API Endpoints

### /catalog

#### GET


##### Summary
Retrieve all articles from the catalog



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |

#### POST


##### Summary
Create new article


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Article](#article) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 409 | Already exists |  |
| 201 | Article created successfully |  |

### /catalog/{article_id}


#### DELETE


##### Summary
Delete an article by ID



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Article not found |  |

#### GET


##### Summary
Retrieve an article by ID



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Article not found |  |

#### PUT


##### Summary
Edit an article by ID


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Article](#article) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 200 | Success |  |
| 404 | Article not found |  |

### /catalog/{article_id}/quantity


#### GET


##### Summary
Retrieve the quantity of an article by ID



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Article not found |  |

#### PUT


##### Summary
Edit the quantity of an article by ID


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Quantity](#quantity) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Article not found |  |

### /seller/{seller_id}


#### DELETE


##### Summary
Delete all articles form a seller



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Seller not found |  |

#### GET


##### Summary
Retrieve all articles from a seller



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Seller not found |  |

## API Schemas

### Quantity

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| quantity | integer |  | Yes |  |

### Article

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| price | number |  | Yes | 1899.95 |
| title | string |  | Yes | Dell® XPS 15inch |
| description | string |  | No | Smallest 15inch laptop in the world. |
| quantity | integer |  | No | 30 |
| seller_id | string |  | Yes | pimoens |
