from werkzeug import datastructures

from flask_restplus import reqparse

image_upload = reqparse.RequestParser()
image_upload.add_argument('image', type=datastructures.FileStorage, location='files', required=True, help='Article image')
