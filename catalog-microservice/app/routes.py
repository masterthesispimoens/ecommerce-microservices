import os

from flask import current_app, request, send_file, url_for
from flask_restplus import Resource, abort, fields

from app import api, db, nice_json
from app.models import Article
from app.parsers import image_upload

article_model = api.model('Article', {
    'title': fields.String(required=True, example='Dell\u00ae XPS 15inch'),
    'description': fields.String(example='Smallest 15inch laptop in the world.'),
    'price': fields.Float(required=True, example=1899.95),
    'quantity': fields.Integer(example=30),
    'seller_id': fields.String(required=True, example='pimoens')
})

quantity_model = api.model('Quantity', {
    'quantity': fields.Integer(required=True, min=1)
})


@api.route('/catalog')
class CatalogResource(Resource):
    def get(self):
        """
        Retrieve all articles from the catalog
        """
        catalog = Article.query.all()
        response = [article.get_json() for article in catalog]
        return nice_json(response)

    @api.expect(article_model, validate=True)
    @api.response(201, 'Article created successfully')
    @api.response(400, 'Bad request')
    @api.response(409, 'Already exists')
    def post(self):
        """
        Create new article
        """
        article = request.json

        article = Article(**article)
        db.session.add(article)
        db.session.commit()

        return nice_json(article.get_json(), 201)


@api.route('/catalog/<article_id>')
@api.doc(params={'article_id': 'An article ID'})
class ArticleResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Article not found')
    def get(self, article_id):
        """
        Retrieve an article by ID
        """
        article = Article.query.filter_by(id=article_id).first()
        if article is not None:
            return nice_json(article.get_json())
        return abort(404)

    @api.expect(article_model, validate=True)
    @api.response(200, 'Success')
    @api.response(400, 'Bad request')
    @api.response(404, 'Article not found')
    def put(self, article_id):
        """
        Edit an article by ID
        """
        article = request.json

        if Article.query.filter_by(id=article_id).update(article):
            db.session.commit()

            article = Article.query.filter_by(id=article_id).first()
            return nice_json(article.get_json())
        else:
            return abort(404)

    @api.response(200, 'Success')
    @api.response(404, 'Article not found')
    def delete(self, article_id):
        """
        Delete an article by ID
        """
        article = Article.query.filter_by(id=article_id).first()

        if article is not None:
            db.session.delete(article)
            db.session.commit()

            catalog = Article.query.all()
            response = [article.get_json() for article in catalog]
            return nice_json(response)
        return abort(404)


@api.route('/catalog/<article_id>/quantity')
@api.doc(params={'article_id': 'An article ID'})
class ArticleQuantityResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Article not found')
    def get(self, article_id):
        """
        Retrieve the quantity of an article by ID
        """
        article = Article.query.filter_by(id=article_id).first()

        if article is not None:
            return nice_json({'quantity': article.quantity})
        return abort(404)

    @api.expect(quantity_model)
    @api.response(200, 'Success')
    @api.response(404, 'Article not found')
    def put(self, article_id):
        """
        Edit the quantity of an article by ID
        """
        article = Article.query.filter_by(id=article_id).first()

        if article is not None:
            article.quantity = request.json['quantity']
            db.session.commit()

            return nice_json(article.get_json())
        return abort(404)


# @api.route('/catalog/<article_id>/image')
# @api.doc(params={'article_id': 'An article ID'})
# class ArticleImageResource(Resource):
#     @api.response(200, 'Success')
#     def get(self, article_id):
#         image = url_for('static', filename='img/default.png')
#         image_extension = 'png'
#
#         return send_file(
#             image,
#             mimetype='image/{extension}'.format(extension=image_extension),
#             cache_timeout=0
#         )
#
#     @api.expect(image_upload)
#     @api.response(200, 'Success')
#     @api.response(400, 'Bad request')
#     @api.response(404, 'Article not found')
#     def post(self, article_id):
#         article = Article.query.filter_by(id=article_id).first()
#         accepted_mimetypes = [{'image/jpeg', 'jpeg'}, ('image/png', 'png'), ('image/svg+xml', 'svg')]
#
#         if article is not None:
#             args = image_upload.parse_args()
#             mimetype = args['image'].mimetype
#
#             if mimetype in accepted_mimetypes:
#                 destination = os.path.join(current_app.config.get('DATA_FOLDER'), 'images/')
#                 if not os.path.exists(destination):
#                     os.makedirs(destination)
#
#                 image = '{}/{}.{}'.format(destination, article.id, mimetype.split('/')[1])
#                 args['image'].save(image)
#             else:
#                 abort(400, 'Unsupported MIME type')
#         else:
#             abort(404)


@api.route('/seller/<seller_id>')
@api.doc(params={'seller_id': 'A seller ID'})
class SellerResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Seller not found')
    def get(self, seller_id):
        """
        Retrieve all articles from a seller
        """
        articles = Article.query.filter_by(seller_id=seller_id).all()

        if articles is not None:
            response = [article.get_json() for article in articles]
            return nice_json(response)
        else:
            return abort(404)

    @api.response(200, 'Success')
    @api.response(404, 'Seller not found')
    def delete(self, seller_id):
        """
        Delete all articles form a seller
        """
        articles = Article.query.filter_by(seller_id=seller_id).all()

        if articles is not None:
            for article in articles:
                db.session.delete(article)
            db.session.commit()

            catalog = Article.query.all()
            response = [article.get_json() for article in catalog]
            return nice_json(response)
        else:
            abort(404)
