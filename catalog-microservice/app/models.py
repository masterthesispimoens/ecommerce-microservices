from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Article(db.Model):

    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(255))
    description = db.Column(db.String)
    image = db.Column(db.String)
    price = db.Column(db.Float)
    quantity = db.Column(db.Integer, default=1)
    seller_id = db.Column(db.String(255))

    def get_json(self):
        return {
            'article_id': self.id,
            'title': self.title,
            'description': self.description,
            'price': self.price,
            'quantity': self.quantity,
            'seller_id': self.seller_id
        }
