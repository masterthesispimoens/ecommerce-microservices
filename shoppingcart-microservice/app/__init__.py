import os

from flask import Flask, make_response, json
from flask_restplus import Api

from app.models import db


def nice_json(arg, status_code=200):
    response = make_response(json.dumps(arg, sort_keys=True, indent=4), status_code)
    response.headers['Content-type'] = "application/json"
    return response


app = Flask(__name__)
app_settings = os.getenv('APP_SETTINGS')
app.config.from_object(app_settings)

with app.app_context():
    db.init_app(app)
    db.create_all()

api = Api(app,
          title='Shopping Cart Microservice API',
          version='1.0',
          description='Shopping cart microservice for the e-commerce microservices architecture'
          )

from app import routes
