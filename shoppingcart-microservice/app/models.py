from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Cart(db.Model):

    __tablename__ = 'carts'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    customer_id = db.Column(db.String(255), unique=True, default=None)
    articles = db.relationship('Article', backref='carts', cascade="all, delete-orphan")

    def get_json(self):
        return {
            'customer_id': self.customer_id,
            'articles': [article.get_json() for article in self.articles]
        }


class Article(db.Model):

    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    article_id = db.Column(db.Integer)
    quantity = db.Column(db.Integer, default=1)
    cart_id = db.Column(db.Integer, db.ForeignKey('carts.id'), nullable=False)

    def get_json(self):
        return {
            'article_id': self.article_id,
            'quantity': self.quantity
        }
