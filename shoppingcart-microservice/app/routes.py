from flask import request
from flask_restplus import Resource, abort, fields

from app import api, db, nice_json
from app.models import Cart, Article


cart_model = api.model('Cart', {
    'customer_id': fields.String(example='pimoens')
})
article_model = api.model('Article', {
    'article_id': fields.Integer(required=True),
    'quantity': fields.Integer(required=True, min=1)
})


@api.route('/carts')
class CartsResource(Resource):
    @api.response(200, 'Success')
    def get(self):
        """
        Retrieve all shopping carts
        """
        carts = Cart.query.all()
        response = [cart.get_json() for cart in carts]
        return nice_json(response)

    @api.expect(cart_model, validate=True)
    @api.response(201, 'Shopping cart created successfully')
    @api.response(400, 'Bad request')
    @api.response(409, 'Already exists')
    def post(self):
        """
        Create a new shopping cart
        """
        cart = request.json
        if 'customer_id' not in cart:
            cart['customer_id'] = None

        _cart = Cart.query.filter_by(customer_id=cart['customer_id']).first()
        if _cart is not None:
            return abort(409, 'A shopping cart already exists for this customer')

        cart = Cart(customer_id=cart['customer_id'])
        db.session.add(cart)
        db.session.commit()

        return nice_json(cart.get_json(), 201)


@api.route('/cart/<customer_id>')
@api.doc(params={'customer_id': 'A customer ID'})
class CartResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Shopping cart not found')
    def get(self, customer_id):
        """
        Retrieve a shopping cart
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()
        if cart is not None:
            return nice_json(cart.get_json())
        return abort(404)

    @api.expect(article_model, validate=True)
    @api.response(201, 'Article added successfully')
    @api.response(400, 'Bad request')
    def post(self, customer_id):
        """
        Add an article to the shopping cart
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()
        if cart is None:
            cart = Cart(customer_id=customer_id)
            db.session.add(cart)
            db.session.commit()

        _article = request.get_json()

        article = Article.query.filter_by(article_id=_article['article_id'], cart_id=cart.id).first()
        if article is not None:
            article.quantity += _article['quantity']
            db.session.commit()

        else:
            _article['cart_id'] = cart.id

            article = Article(**_article)
            db.session.add(article)
            db.session.commit()

        return nice_json(cart.get_json(), 201)

    @api.response(200, 'Success')
    @api.response(404, 'Shopping cart not found')
    def delete(self, customer_id):
        """
        Delete a shopping cart
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()
        if cart is not None:
            db.session.delete(cart)
            db.session.commit()

            carts = Cart.query.all()
            response = [cart.get_json() for cart in carts]
            return nice_json(response)
        else:
            return abort(404)
        
        
@api.route('/cart/<customer_id>/empty')
@api.doc(params={'customer_id': 'A customer ID'})
class CartEmptyResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Shopping cart not found')
    def post(self, customer_id):
        """
        Empty a shopping cart
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()

        if cart is not None:
            for article in cart.articles:
                db.session.delete(article)
            db.session.commit()

            cart = Cart.query.filter_by(customer_id=customer_id).first()
            return nice_json(cart.get_json())
        return abort(404)


@api.route('/cart/<customer_id>/article/<article_id>')
@api.doc(params={'customer_id': 'A customer ID', 'article_id': 'An article ID'})
class CartArticleResource(Resource):
    @api.response(200, 'Success')
    @api.response(404, 'Shopping cart or article not found')
    def get(self, customer_id, article_id):
        """
        Retrieve a shopping cart article
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()
        if cart is not None:
            article = Article.query.filter_by(article_id=article_id, cart_id=cart.id).first()
            if article is not None:
                return nice_json(article.get_json())
            else:
                return abort(404, 'Article not found in shopping cart')
        else:
            abort(404, 'Shopping cart not found')

    @api.expect(article_model, validate=True)
    @api.response(200, 'Success')
    @api.response(400, 'Bad request')
    @api.response(404, 'Shopping cart or article not found')
    def put(self, customer_id, article_id):
        """
        Edit a shopping cart article
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()

        if cart is not None:
            article = request.get_json()

            if Article.query.filter_by(article_id=article_id, cart_id=cart.id).update(article):
                db.session.commit()

                cart = Cart.query.filter_by(customer_id=customer_id).first()
                return nice_json(cart.get_json())
            else:
                return abort(404, 'Article not found in shopping cart')
        else:
            return abort(404, 'Shopping cart not found')

    @api.response(200, 'Success')
    @api.response(404, 'Shopping cart or article not found')
    def delete(self, customer_id, article_id):
        """
        Delete a shopping cart article
        """
        cart = Cart.query.filter_by(customer_id=customer_id).first()

        if cart is not None:
            article = Article.query.filter_by(article_id=article_id, cart_id=cart.id).first()
            if article is not None:
                db.session.delete(article)
                db.session.commit()

                cart = Cart.query.filter_by(customer_id=customer_id).first()
                return nice_json(cart.get_json())
            else:
                return abort(404, 'Article not found in shopping cart')
        else:
            return abort(404, 'Shopping cart not found')


@api.route('/cart/<customer_id>/merge')
@api.doc(params={'customer_id': 'A customer ID'})
class MergeCartResource(Resource):
    @api.expect(cart_model, validate=True)
    @api.response(200, 'Success')
    @api.response(400, 'Bad request')
    def post(self, customer_id):
        """
        Merge shopping cart with customer cart
        """
        merge_cart = request.json

        cart = Cart.query.filter_by(customer_id=customer_id).first()
        if cart is None:
            cart = Cart(customer_id=customer_id)
            db.session.add(cart)
            db.session.commit()

        for _article in merge_cart['articles']:
            article = Article.query.filter_by(article_id=_article['article_id'], cart_id=cart.id).first()
            if article is not None:
                article.quantity += _article['quantity']
                db.session.commit()

            else:
                _article['cart_id'] = cart.id

                article = Article(**_article)
                db.session.add(article)
                db.session.commit()

        cart = Cart.query.filter_by(customer_id=customer_id).first()
        return nice_json(cart.get_json())
