
# Shopping Cart Microservice API [v1.0]
Shopping cart microservice for the e-commerce microservices architecture

## API Endpoints

### /cart/{customer_id}


#### DELETE


##### Summary
Delete a shopping cart



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Shopping cart not found |  |

#### POST


##### Summary
Add an article to the shopping cart


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Article](#article) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 201 | Article added successfully |  |

#### GET


##### Summary
Retrieve a shopping cart



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Shopping cart not found |  |

### /cart/{customer_id}/article/{article_id}

#### GET


##### Summary
Retrieve a shopping cart article



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Shopping cart or article not found |  |


#### PUT


##### Summary
Edit a shopping cart article


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Article](#article) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 200 | Success |  |
| 404 | Shopping cart or article not found |  |

#### DELETE


##### Summary
Delete a shopping cart article



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Shopping cart or article not found |  |

### /cart/{customer_id}/empty


#### POST


##### Summary
Empty a shopping cart



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |
| 404 | Shopping cart not found |  |

### /cart/{customer_id}/merge


#### POST


##### Summary
Merge shopping cart with customer cart


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Cart](#cart) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 200 | Success |  |

### /carts

#### GET


##### Summary
Retrieve all shopping carts



##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Success |  |

#### POST


##### Summary
Create a new shopping cart


##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ------ |
| payload | body |  | Yes | [Cart](#cart) |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 400 | Bad request |  |
| 409 | Already exists |  |
| 201 | Shopping cart created successfully |  |

## API Schemas

### Cart

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| customer_id | string |  | No | pimoens |

### Article

| Name | Type | Description | Required | Example |
| ---- | ---- | ----------- | -------- | ------- |
| article_id | integer |  | Yes |  |
| quantity | integer |  | Yes |  |
