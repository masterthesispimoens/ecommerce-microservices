# E-Commerce Microservices Architecture
## Implementation of an E-Commerce webshop written in Flask

Microservice implementation used as test environment during master thesis on automatic scalability evaluation.

## Microservices
* E-Commerce Web Application [:5000]
* Customer Microservice [:5010]
* Catalog Microservice [:5011]
* Shopping Cart Microservice  [:5012]
* Order Management Microservice [:5013]

![microservices-architecture](images/musiccorp-microservices.png)

## Deployment

### Docker Compose

Upon successful building of microservices, you can now build Docker images and run containers Docker host on your machine, Note: Prior to execute below instructions, ensure you have started 'Docker Quickstart Terminal'. If you are new to docker, read more about Docker at, https://docs.docker.com/engine/getstarted/ Excute below commands in sequence on 'Docker Quickstart Terminal',

```
cd <path-to-project-root>
docker-compose build --no-cache
```
Above command may take time for first time, as it needs to download base images. All docker containers are based on light-weight LinuxOS called 'Alpine Linux', which is hardly ~5MB.

Issue following command, to run Docker containers,

```
docker-compose up
```
Above command starts all Microsevices Docker containers as specified in 'docker-compose.yml' file. Important Note: Some Microservices may not start properly due to interdependcy on infrastructure Microservices' containers. Unfortunately, docker-compose command will NOT wait till dependent containers started. It will start all containers simultaneously. You may need to re-start failed containers manually. I recommend to use 'Kitematic (Alpha)' console shipped with 'Docker Toolbox' to restart failed containers.

You can run the above commands as one to build and run the containers in detached mode,
```
docker-compose up -d --build
```
And bring the docker containers down (when needed) using below commands,
```
docker-compose down --remove-orphans
```

### Kompose

The project can be deployed on a Kubernetes architecture automatically using Kompose, a Docker Compose to Kubernetes converter.
Read more at https://github.com/kubernetes/kompose

After installation and cloning of the repository, simply run,

```
kompose up
```

Don't forget to provide the necessary persistent volumes for your Kubernetes cluster.

The deployment can also be done manually by running all the configuration files from the /kubernetes directory.
These have been created using,

```
kompose convert
```

Finally create a NodePort service to make the ecommerce-webapp service avaiable:

```
kubectl expose deployment ecommerce-webapp --type=NodePort --name=ecommerce-webapp-port
```

## Service Mesh and Monitoring
Running the application with Istio included, requires deployment using Helm.

### Pre-requisites
1. You need to have a Kubernetes cluster up and running
2. Have Helm client and tiller configured in your cluster. Refer [here](https://docs.helm.sh/using_helm/#installing-helm)
3. Clone the official [Istio repo](https://github.com/istio/istio)
```
helm install install/kubernetes/helm/istio-init --namespace istio-system
helm install install/kubernetes/helm/istio --name istio --namespace istio-system
```

**Note:** See install/kubernetes/helm/istio/values.yaml to enable extensions (e.g. Kiali, Grafana..).
This is explained in the installation [documentation](https://github.com/istio/istio/blob/44798c747a5a4483bafc810c35e568494d78cc99/install/kubernetes/helm/istio/README.md).

### Deployment
To simplify this process, a bash script has been provided in the [helm directory](https://bitbucket.org/masterthesispimoens/ecommerce-microservices/src/master/helm/).
You can run it by entering the following commands,
```
cd helm
sudo chmod +x deploy-services-istio.sh
./deploy-services.istio.sh
```
**Note:** Don't forget to search for all namespaces in the Kubernetes dashboard when deploying witth this script.
Every microservice has its own namespace.

To remove all the release with a single command, simply run,
```
helm ls --all --short | xargs -L1 helm delete --purge
```
To remove the Istio installation from the Kubernetes cluster, run,
```
helm del --purge istio
```
