import argparse
import requests


def main(args):
    # Customer API
    CUSTOMER_URL = '{host}:{port}'.format(host=args.host, port=args.customer_port)
    # Catalog API
    CATALOG_URL = '{host}:{port}'.format(host=args.host, port=args.catalog_port)
    # Shopping cart API
    SHOPPINGCART_URL = '{host}:{port}'.format(host=args.host, port=args.cart_port)
    # Order management API
    ORDER_URL = '{host}:{port}'.format(host=args.host, port=args.order_port)

    for customer in requests.get(CUSTOMER_URL + '/customers').json():
        requests.delete(CUSTOMER_URL + '/customer/{customer}'.format(customer=customer['username']))

    for article in requests.get(CATALOG_URL + '/catalog').json():
        requests.delete(CATALOG_URL + '/catalog/{article}'.format(article=article['article_id']))

    for cart in requests.get(SHOPPINGCART_URL + '/carts').json():
        requests.delete(SHOPPINGCART_URL + '/cart/{customer}'.format(customer=cart['customer_id']))

    for order in requests.get(ORDER_URL + '/orders').json():
        requests.delete(ORDER_URL + '/order/{order}'.format(order=order['order_id']))

    print('Customers:\t', requests.get(CUSTOMER_URL + '/customers').json())
    print('Articles:\t', requests.get(CATALOG_URL + '/catalog').json())
    print('Carts:\t\t', requests.get(SHOPPINGCART_URL + '/carts').json())
    print('Orders:\t\t', requests.get(ORDER_URL + '/orders').json())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--host', default='http://localhost', help='Host address (default: http://localhost')
    parser.add_argument('--customer-port', default=5010, help='Port of Customer Microservice API (default: 5010)')
    parser.add_argument('--catalog-port', default=5011, help='Port of Catalog Microservice API (default: 5011)')
    parser.add_argument('--cart-port', default=5012, help='Port of Shoppingcart Microservice API (default: 5012)')
    parser.add_argument('--order-port', default=5013, help='Port of  Order Management Microservice API (default: 5013)')

    args = parser.parse_args()
    main(args)
