from grafanalib.core import *

dashboard = Dashboard(
    title='RED Dashboard',
    templating=Templating(
        [
            Template(
                name='source',
                label='Source',
                dataSource='Prometheus',
                query='label_values(source_workload)'
            ),
            Template(
                name='destination',
                label='Destination',
                dataSource='Prometheus',
                query='label_values({source_workload="$source"}, destination_workload)'
            )
        ]
    ),
    rows=[
        Row(
            panels=[
                SingleStat(
                    title='Request Volume',
                    transparent=True,
                    dataSource='Prometheus',
                    span=2,
                    valueName=VTYPE_CURR,
                    targets=[
                        Target(
                            expr='round(sum(irate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination"}[5m])), 0.001)'
                        )
                    ],
                    sparkline=SparkLine(
                        show=True,
                        full=True
                    ),
                    format=OPS_FORMAT
                ),
                SingleStat(
                    title='Success Rate (non-5XX)',
                    transparent=True,
                    dataSource='Prometheus',
                    span=2,
                    valueName=VTYPE_CURR,
                    targets=[
                        Target(
                            expr='sum(irate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code!~"5.*"}[5m])) '
                                 '/ sum(irate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination"}[5m]))'
                        )
                    ],
                    sparkline=SparkLine(
                        show=True,
                        full=True
                    ),
                    format=PERCENT_UNIT_FORMAT
                ),
                Graph(
                    title='Latency',
                    transparent=True,
                    dataSource='Prometheus',
                    span=8,
                    targets=[
                        Target(
                            expr='histogram_quantile(0.5, sum(rate(istio_request_duration_seconds_bucket{'
                                 'reporter="source", source_workload="$source", '
                                 'destination_workload="$destination"}[1m])) by (le))',
                            legendFormat='P50'
                        ),
                        Target(
                            expr='histogram_quantile(0.95, sum(rate(istio_request_duration_seconds_bucket{'
                                 'reporter="source", source_workload="$source", '
                                 'destination_workload="$destination"}[1m])) by (le))',
                            legendFormat='P95'
                        ),
                        Target(
                            expr='histogram_quantile(0.99, sum(rate(istio_request_duration_seconds_bucket{'
                                 'reporter="source", source_workload="$source", '
                                 'destination_workload="$destination"}[1m])) by (le))',
                            legendFormat='P99'
                        )
                    ],
                    yAxes=single_y_axis(
                        label='Latency (Seconds)',
                        format=SECONDS_FORMAT
                    )
                )
            ]
        ),
        Row(
            panels=[
                SingleStat(
                    title='Succesful Requests [2XX-3XX]',
                    transparent=True,
                    dataSource='Prometheus',
                    span=4,
                    valueName=VTYPE_MAX,
                    targets=[
                        Target(
                            expr='sum(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code=~"2[0-9][0-9]|3[0-9][0-9]"'
                                 '})'
                        )
                    ]
                ),
                Graph(
                    title='Succesful Requests [2XX-3XX]',
                    transparent=True,
                    dataSource='Prometheus',
                    span=8,
                    targets=[
                        Target(
                            expr='sum(rate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code=~"2[0-9][0-9]|3[0-9][0-9]"}[1m]))',
                            legendFormat='total'
                        ),
                        Target(
                            expr='sum(rate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code=~"2[0-9][0-9]|3[0-9][0-9]"'
                                 '}[1m])) by (response_code)',
                            legendFormat='{{response_code}}'
                        )
                    ],
                    yAxes=single_y_axis(
                        label='Requests Per Second (RPS)',
                        format=OPS_FORMAT
                    )
                )
            ]
        ),
        Row(
            panels=[
                SingleStat(
                    title='Error Requests [4XX-5XX]',
                    transparent=True,
                    dataSource='Prometheus',
                    span=4,
                    valueName=VTYPE_MAX,
                    targets=[
                        Target(
                            expr='sum(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code=~"4[0-9][0-9]|5[0-9][0-9]"'
                                 '})'
                        )
                    ]
                ),
                Graph(
                    title='Error Requests [4XX-5XX]',
                    transparent=True,
                    dataSource='Prometheus',
                    span=8,
                    targets=[
                        Target(
                            expr='sum(rate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code=~"4[0-9][0-9]|5[0-9][0-9]"}[1m]))',
                            legendFormat='total'
                        ),
                        Target(
                            expr='sum(rate(istio_requests_total{'
                                 'reporter="source", '
                                 'source_workload="$source", '
                                 'destination_workload="$destination", '
                                 'response_code=~"4[0-9][0-9]|5[0-9][0-9]"'
                                 '}[1m])) by (response_code)',
                            legendFormat='{{response_code}}'
                        )
                    ],
                    yAxes=single_y_axis(
                        label='Requests Per Second (RPS)',
                        format=OPS_FORMAT
                    )
                )
            ]
        )
    ]
).auto_panel_ids()
