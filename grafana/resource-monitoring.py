from grafanalib.core import *

dashboard = Dashboard(
    title='Resource Monitoring',
    templating=Templating(
        [
            Template(
                name='namespace',
                dataSource='Prometheus',
                query='label_values(namespace)'
            ),
            Template(
                name='pod',
                dataSource='Prometheus',
                query='label_values({namespace="$namespace"}, pod_name)',
                multi=True,
                includeAll=True
            )
        ]
    ),
    rows=[
        Row(
            panels=[
                Graph(
                    title='Memory Usage',
                    transparent=True,
                    dataSource='Prometheus',
                    targets=[
                        Target(
                            expr='sum ('
                                 'rate(container_memory_usage_bytes{'
                                 'container_name!="POD", container_name!="", pod_name="$pod"}[1m])'
                                 ') by (container_name)',
                            legendFormat='{{container_name}}'
                        )
                    ],
                    yAxes=single_y_axis(
                        label='Memory Usage (Bytes)',
                        format=BYTES_FORMAT
                    )
                ),
                Graph(
                    title='CPU Usage',
                    transparent=True,
                    dataSource='Prometheus',
                    targets=[
                        Target(
                            expr='sum ('
                                 'rate(container_cpu_usage_seconds_total{'
                                 'container_name!="POD", container_name!="", pod_name="$pod"}[1m])'
                                 ') by (container_name)',
                            legendFormat='{{container_name}}'
                        )
                    ],
                    yAxes=single_y_axis(
                        label='CPU Usage (Seconds)',
                        format=SECONDS_FORMAT
                    )
                )
            ]
        ),
    ]
).auto_panel_ids()