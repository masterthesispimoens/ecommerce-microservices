#!/usr/bin/env bash

ython3 swagger2markdown.py -i 'http://localhost:5010/swagger.json' -t 'swagger-template.j2' -o '../customer-microservice/README.md'
python3 swagger2markdown.py -i 'http://localhost:5011/swagger.json' -t 'swagger-template.j2' -o '../catalog-microservice/README.md'
python3 swagger2markdown.py -i 'http://localhost:5012/swagger.json' -t 'swagger-template.j2' -o '../shoppingcart-microservice/README.md'
python3 swagger2markdown.py -i 'http://localhost:5013/swagger.json' -t 'swagger-template.j2' -o '../order-microservice/README.md'